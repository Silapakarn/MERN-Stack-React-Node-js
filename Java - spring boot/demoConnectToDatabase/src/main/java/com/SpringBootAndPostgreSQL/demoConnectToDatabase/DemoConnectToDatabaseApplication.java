package com.SpringBootAndPostgreSQL.demoConnectToDatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoConnectToDatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoConnectToDatabaseApplication.class, args);
	}

}
