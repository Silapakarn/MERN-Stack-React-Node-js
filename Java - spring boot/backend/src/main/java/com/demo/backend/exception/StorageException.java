package com.demo.backend.exception;

public class StorageException extends RuntimeException {

    public StorageException(String string) {
        super(string);
    }
}
