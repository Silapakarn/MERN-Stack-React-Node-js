package com.in28minutes.junit;

public class SwitchCaseJava {
    public static void main(String[] args) {
        int position = 1;

        switch (position) {
            case 0:
                System.out.println("Postion is: " + position);
                break;
            case 1:
                System.out.println("Postion is: " + position);
                break;
            case 2:
                System.out.println("Postion is: " + position);
                break;
            default: 
                System.out.println("position is n/a");
                break;
        }
    }
}
