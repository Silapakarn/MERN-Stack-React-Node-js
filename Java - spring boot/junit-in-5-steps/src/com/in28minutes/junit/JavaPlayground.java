package com.in28minutes.junit;

public class JavaPlayground {
    public static void main(String[] args) {

        int love = 3000;
        System.out.println("Dragon !" + love);

        //Boolean
        boolean isLove = true;
        if(isLove == true){
            System.out.println("Love is: " + isLove);
        }else{
            System.out.println("Love is: " + isLove);
        }   

        // character
        char i = 't';
        System.out.println("\n" + i);
    }
}
