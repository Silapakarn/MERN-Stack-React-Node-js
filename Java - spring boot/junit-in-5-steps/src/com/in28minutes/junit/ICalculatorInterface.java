package com.in28minutes.junit;

public interface ICalculatorInterface {

    public String combine(double a, double c);

    public String minus(double a, double c);

    public String multiply(double a, double c);

}
