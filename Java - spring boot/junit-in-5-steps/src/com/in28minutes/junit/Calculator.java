package com.in28minutes.junit;

public class Calculator {
        public String combine(double a, double b){
            return "Combine Result: " + (a+b);
        }

        public String minus(double a, double b){
            return "Minus Result: " + (a-b);
        }

        public String multiply(double a, double b){
            return "Multiply Result: " + (a*b);
        }

        public String divide(double a, double b){
            return "Divide Result: " + (a/b);
        }
}
