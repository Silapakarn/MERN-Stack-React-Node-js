module github.com/AnuchitO/db

go 1.18

require github.com/proullon/ramsql v0.0.0-20220319205533-582afa8a5528

require github.com/go-gorp/gorp v2.2.0+incompatible // indirect
