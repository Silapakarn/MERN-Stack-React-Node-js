export class CreateCoffeeDto {
    id:number;
    type: string;
    price: number;
    duration: number;
    stock: number;
    sweetness: string;
    option: boolean;
}
