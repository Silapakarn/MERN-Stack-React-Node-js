import logo from './logo.svg';
import './App.css';
import { CompA } from './CompA';
import { CompB } from './CompB';


function App() {
  return (
    <div className="App">
    Hello World!

    <CompA />
    <CompB />
    </div>
  );
}

export default App;
